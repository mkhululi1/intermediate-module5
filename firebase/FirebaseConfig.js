import * as firebase from 'firebase/app';
import {getFirestore} from '@firebase/firestore';
import * as firebaseAuth from 'firebase/auth'; 


const firebaseConfig = {
  apiKey: "AIzaSyCbnyVUoBmOOOy-0krtn6PiPtwKSIZ4Fh0",
  authDomain: "intermediate-module5.firebaseapp.com",
  projectId: "intermediate-module5",
  storageBucket: "intermediate-module5.appspot.com",
  messagingSenderId: "963044858047",
  appId: "1:963044858047:web:418a0c50c31d42953b4e0a"
};

  const app = firebase.initializeApp(firebaseConfig)  

  export const db = getFirestore(app)
  
  export const auth = firebaseAuth;