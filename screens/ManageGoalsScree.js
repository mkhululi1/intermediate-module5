import { StatusBar } from 'expo-status-bar';
import React, {useEffect, useState} from 'react';
import { Button, StyleSheet, Text, View, TextInput, FlatList, TouchableOpacity, ScrollView } from 'react-native';
import { db } from '../firebase/FirebaseConfig';
import { collection, doc, getDocs, addDoc, deleteDoc,where, query  } from '@firebase/firestore'
import {getAuth} from 'firebase/auth'

const ManageGoalsScreen = (props) => {

    const [goals, setGoals] = useState([{goal:"Pass"},{goal:"Pass"},{goal:"Pass"},{goal:"Pass"},{goal:"Pass"},])
    const [newGoal, setNewGoal] = useState("")
    const auth = getAuth()
    useEffect(() => {
        goals.forEach(item =>(
            console.log(item.goal)
        ))
        console.log(goals)
    },[])
  useEffect(()=>{

    const fetchGoals = async() => {
        try{
            const snapshot = query(collection(db, "goals"),where("creator","==", auth.currentUser.uid))
            const querySnapshot = await getDocs(snapshot)
            let docs = querySnapshot.docs.map(doc => {
                const data = doc.data();
                const id = doc.id;
                return { id, ...data }
            });
            if(docs){
                setGoals(docs)

            }
        }catch(err){
            console.log(err)
        }
        
    }
    fetchGoals()
  },[])

  const addGoal = async () => {
    try{
        const docRef = query(collection(db, "goals"))
        const save = await addDoc(docRef, {
            goal: newGoal, 
            creator:auth.currentUser.uid, 
        })
        console.log(save)
        setGoals([...goals,{goal:newGoal, id:save.id, creator:auth.currentUser.uid}])
        setNewGoal("")
    }catch(err){
        return console.log(err)
    }
  }

  const removeGoal = async (goalId) => {
    const docRef = query(doc(db, "goals",goalId))
    await deleteDoc(docRef)

    const updatedGoals = goals.filter(goal => goal.id !== goalId)
    setGoals(updatedGoals)
  }

  
    return (
      <View style={styles.container}>
        <View style={styles.roow}>
            <TextInput style={styles.textInpt} value={newGoal} onChangeText={(val => setNewGoal(val))}/>
            <TouchableOpacity onPress={addGoal} disabled={newGoal.length < 2}>
                <Text style={[styles.appButtonText ,{color:'#1a73e8', borderColor:'#1a73e8'}]} >Add</Text>
            </TouchableOpacity>
        </View>
        <ScrollView style={{width:'95%'}}>
            <FlatList
                data={goals}
                renderItem={({item, index}) =>(
                    <View style={[styles.roow, {justifyContent:'space-between', borderColor: '#ccc',borderWidth: 1,borderRadius:5, paddingHorizontal:10, marginVertical:10}]} key={index}>
                        <Text style={{fontSize:20 }}>{item.goal}</Text>
                        <TouchableOpacity>
                            <Text onPress={() => {removeGoal(item.id)}} style={[styles.appButtonText ,{color:'crimson', borderColor:'crimson'}]}>X</Text>
                        </TouchableOpacity>
                    </View>
                    )
                }
            />

        </ScrollView>
      </View>
    );
  }
  
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      // backgroundColor: '#fff',
      backgroundColor: 'white',
      alignItems: 'center',
      justifyContent: 'center',
    },
    actions: {
      borderColor: "gray",
      borderWidth: 2,
      // display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      padding: 5
    },
    btn : {
      flex: 1,
      padding: 10,
      // color:'red',
      width: 20,
      height: 4,
      // accessibilityLabel:"Learn more about this purple button",
      color:"black",
      borderBottomColor: 'yellow',
    },
    appButtonText:{
        borderWidth: 1, 
        borderRadius: 5,
        paddingHorizontal:7, 
        paddingVertical:2, 
        marginVertical:12, 
        justifyContent:'center',
        fontSize: 14,
    },
    textInpt: {
        borderBottomWidth: 2,
        borderBottomColor: '#ddd',
        margin: 15,
        fontSize:20
    },
    roow:{
        display:'flex', 
        flexDirection:'row', 
        alignItems:'center'
    },
  });
  

export default ManageGoalsScreen
