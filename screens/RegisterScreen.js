import React, { useState } from 'react'
import { StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native'
import { db } from '../firebase/FirebaseConfig'
import {doc,setDoc } from '@firebase/firestore'
import {getAuth, createUserWithEmailAndPassword, updateProfile} from 'firebase/auth'

const RegisterScreen = ({navigation}) => {
    const auth = getAuth()
    const [name, setName] = useState("")
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")


    const registerHandler = async() => {
        try{
            try{
                const newUser = await createUserWithEmailAndPassword(auth, email, password)
                await updateProfile(auth.currentUser, {displayName:name})
                console.log(newUser)
            }catch(err){
                return console.log(err)
            }

            try{
                const docRef = doc(db, "users", auth.currentUser.uid)
                await setDoc(docRef, {
                    email: email, 
                    username: name.toLowerCase(), 
                })
                navigation.navigate("HomeScreen")
            }catch(err){
                return console.log(err)
            }
            
             
        console.log("Registered")
        loginHandler()
        }catch(err){
            console.log(err)
        }
    }
    return (
        <View style={{backgroundColor:'#fff', justifyContent: 'center', alignItems: 'center'}}>
            <View style={styles.register}>
                <Text style={{fontWeight:'bold', fontSize:18, color:'#555555'}}>SIGN UP</Text>
                <View>
                    <View>
                        <TextInput style={styles.input} placeholder="Name" onChangeText={(value) => setName(value)}/>
                        <TextInput style={styles.input} placeholder="Email" onChangeText={(value) => setEmail(value)} />
                        <TextInput style={styles.input} passwordRules={true} minLength={6} 
                        placeholder="Password" onChangeText={(value) => setPassword(value)} />
                    </View>
                    <View style={{display:'flex', flexDirection:'row', justifyContent:'space-between'}}>
                        
                        <TouchableOpacity style={styles.btnContainer}>
                            <Text style={styles.appButtonText} onPress={registerHandler}>Sign Up</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.btnContainer, {backgroundColor:'#fff', borderColor:'#ffa500',borderWidth:2}]} 
                        onPress={() => {navigation.navigate("LoginScreen")}}>
                            <Text style={[styles.appButtonText ,{color:'#ffa500'}]}>Sign In</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </View>
    )
}

export default RegisterScreen

const styles = StyleSheet.create({
    register: {
        width: '90%',
        borderColor: '#007bff',
        borderWidth: 2,
        borderRadius: 5,
        justifyContent: 'center',
        paddingHorizontal: 15,
        paddingVertical: 30,
        width: '90%',
        marginTop: 22
    },
    btnContainer:{
        paddingHorizontal: 6,
        paddingVertical: 4,
        elevation: 6,
        borderRadius:6,
        backgroundColor: '#007bff'
    },
    appButtonText: {
        fontSize: 14,
        color: "#fff",
        fontWeight: "bold",
        alignSelf: "center",
        // textTransform: "uppercase"
      },
      input:{
        borderColor: '#007bff',
        borderWidth: 0.5,
        padding: 5,
        marginVertical:10,
        
        // width: '100%'
      }
})
