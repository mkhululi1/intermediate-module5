import React from 'react';
import { Button, StyleSheet, Text, View} from 'react-native';
import {getAuth, signOut} from 'firebase/auth'
const auth = getAuth()

const HomeScreen = (props) => {
    const auth = getAuth()

    const logout = async () => {
        await signOut(auth)
        props.navigation.navigate("LoginScreen")
    }
  
    return (
      <View style={styles.container}>
        <View style={styles.roow}>
                <Text style={[styles.appButtonText ,{color:'#1a73e8', fontSize:20, borderColor:'#fff'}]} >This App helps you manage life goals</Text>
        </View>
        <View>
            <Button title="Continue" onPress={() =>props.navigation.navigate("ManageGoalsScreen")}/>
        </View>

        <View style={[styles.roow, {marginTop:20}]}>
            <Text onPress={logout} style={[styles.appButtonText ,{color:'crimson', fontSize:20, borderColor:'crimson'}]} >Sign Out</Text>
        </View>
        
      </View>
    );
  }
  
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      // backgroundColor: '#fff',
      backgroundColor: 'white',
      alignItems: 'center',
      justifyContent: 'center',
    },
    appButtonText:{
        borderWidth: 1, 
        borderRadius: 5,
        paddingHorizontal:7, 
        paddingVertical:2, 
        marginVertical:12, 
        justifyContent:'center',
        fontSize: 14,
    },

    roow:{
        display:'flex', 
        flexDirection:'row', 
        alignItems:'center'
    },
  });
  

export default HomeScreen
