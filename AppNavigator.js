import React, {useState, useEffect} from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import {NavigationContainer } from '@react-navigation/native'
import LoginScreen from './screens/LoginScreen'
import RegisterScreen from './screens/RegisterScreen'
import HomeScreen from './screens/HomeScreen'
import {getAuth} from 'firebase/auth'
import ManageGoalsScreen from './screens/ManageGoalsScree'

const Stack = createStackNavigator();
const AppNavigator = () => {
  const [isLogged, setIsLogged] = useState(false)

  const auth = getAuth()
  useEffect(() => {
       auth.onAuthStateChanged(async(user) => {
        
          if(user){
                setIsLogged(true)
          }
        })
        console.log("user")
  },[isLogged, auth])

  if(isLogged){
    return (
      <NavigationContainer>
          <Stack.Navigator>
              <Stack.Screen name="HomeScreen" component={HomeScreen} options={{headerTitle: "Dashboard"}}/>
              <Stack.Screen name="LoginScreen" component={LoginScreen} options={{headerTitle: "Sign In"}}/>
              <Stack.Screen name="RegisterScreen" component={RegisterScreen} options={{headerTitle: "Sign Up"}}/>
              <Stack.Screen name="ManageGoalsScreen" component={ManageGoalsScreen} options={{headerTitle: "Manage Life Goals"}}/>
          </Stack.Navigator>
  
      </NavigationContainer>
    )
  }
  return (
    <NavigationContainer>
        <Stack.Navigator>
            <Stack.Screen name="LoginScreen" component={LoginScreen} options={{headerTitle: "Sign In"}}/>
            <Stack.Screen name="RegisterScreen" component={RegisterScreen} options={{headerTitle: "Sign Up"}}/>
            <Stack.Screen name="HomeScreen" component={HomeScreen} options={{headerTitle: "Dashboard"}}/>
            <Stack.Screen name="ManageGoalsScreen" component={ManageGoalsScreen} options={{headerTitle: "Manage Life Goals"}}/>
        </Stack.Navigator>

    </NavigationContainer>
  )
}

export default AppNavigator